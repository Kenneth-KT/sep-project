package system.command;
import java.util.List;
import java.util.Scanner;

import system.Employee;



public class ShowSalary implements Command {
	private static ShowSalary instance;
	private ShowSalary(){
		
	}
	public static ShowSalary getInstance(){
		instance = (instance == null)?new ShowSalary():instance;
		return instance;
	}
	@Override
	public void execute(List<Employee> employees,String[] param) {
		int year = Integer.parseInt(param[1]);
		int month = Integer.parseInt(param[0]);
		printSalaryHeader();
		for(Employee e:employees){
			printRow(e,e.getSalary(year, month));
		}
	}
	private void printSalaryHeader(){
		System.out.printf("%15s\t\t%10s","Employee name","Salary");
	}
	private void printRow(Employee e,double salary){
		System.out.printf("\n%15s\t\t%10f",e.getName(),salary);
	}
	@Override
	public String toString(){
		return "Show Salary";
	}
	@Override
	public String[] getParam(Scanner sc) {
		String[] params = null;
		if(sc != null){
			System.out.print("Please enter the month(MM-yyyy):");
			String line = sc.nextLine();
			params = line.split("-");
		}
		return params;
	}
}