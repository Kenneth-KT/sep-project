package testCase;

import static org.junit.Assert.*;

import org.junit.Test;
import system.Calendar;
import system.NoPayLeave;

public class NoPayLeaveTest {
	@Test
	public void testFullTimeSalaryAdjustment() {
		NoPayLeave npl = new NoPayLeave(Calendar.getInstance().getDay(2015, 10, 29),Calendar.getInstance().getDay(2015, 11, 02));
		double adjustment = npl.getFullTimeSalaryAdjustment(2015, 10, npl.getStart(), npl.getEnd(), 8000);
		assertEquals(adjustment, -800, 0);
	}
	@Test
	public void testPartTimeSalaryAdjustment() {
		NoPayLeave npl = new NoPayLeave(Calendar.getInstance().getDay(2015, 10, 29),Calendar.getInstance().getDay(2015, 11, 02));
		double adjustment = npl.getPartTimeSalaryAdjustment(2015, 10, npl.getStart(), npl.getEnd(), 50);
		assertEquals(adjustment, 0, 0);
	}
	@Test
	public void testToString() {
		NoPayLeave npl = new NoPayLeave(Calendar.getInstance().getDay(2015, 10, 15),Calendar.getInstance().getDay(2015, 10, 21));
		assertEquals(npl.toString(), "15-Oct-2015 - 21-Oct-2015 [No-Pay Leave]");
	}
	
}
