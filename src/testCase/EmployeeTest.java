package testCase;

import static org.junit.Assert.*;

import org.junit.Test;

import system.AnnualLeave;
import system.Calendar;
import system.Day;
import system.Employee;
import system.EmploymentHistory;
import system.FullTimeEmploymentHistory;
import system.LeaveRecord;
import system.Position;
import system.PartTimeEmploymentHistory;

public class EmployeeTest {

	@Test
	public void test_getName() {
		Employee emp = new Employee("Jar", new Day(2014, 10, 1));
		String ename = emp.getName();
		assertEquals(ename, "Jar");
	}

	@Test
	public void test_getBirthday() {
		Employee emp = new Employee("Carmen", new Day(2014, 10, 1));
		Day dob = emp.getBirthday();
		String strDOB = dob.toString();
		assertEquals(strDOB, "1-Oct-2014");
	}

	@Test
	public void test_getSalary() {
		double result;
		Employee emp = new Employee("Austin", new Day(1992, 10, 1));
		Day startDate = new Day(2015, 1, 1);
		Position position = new Position("Engineer");
		EmploymentHistory eh = new FullTimeEmploymentHistory(startDate,
				position, 15000.0);
		emp.addEmploymentHistory(eh);
		result = emp.getSalary(2015, 2);
		assertEquals(result, 15000.0, 1);
	}

	@Test
	public void test_getSalary2() {
		double result;
		Employee emp = new Employee("Austin",Calendar.getInstance().getDay(1992, 10, 1));
		Day startDate = Calendar.getInstance().getDay(2015, 1, 1);
		Position position = new Position("Engineer");
		EmploymentHistory eh = new PartTimeEmploymentHistory(startDate,
				position, 15000.0);
		emp.addEmploymentHistory(eh);
		result = emp.getSalary(2015, 2);
		System.out.print(result);
		assertEquals(result, 0, 1);
		// problem cased
	}

	@Test
	public void testAddEmploymentHistoryEmploymentHistory() {
		int result;
		Employee emp = new Employee("Austin", new Day(1992, 10, 1));
		Day startDate = new Day(2015, 1, 1);
		Position position = new Position("Engineer");
		EmploymentHistory eh = new FullTimeEmploymentHistory(startDate,
				position, 15000.0);
		emp.addEmploymentHistory(eh);
		result = emp.getEmploymentHistorys().size();
		assertEquals(result, 1);
	}

	@Test
	public void testMatch() {
		boolean result;
		Employee emp = new Employee("Austin", new Day(1992, 10, 1));
		String name = "Austin";
		result = emp.match(name);
		assertEquals(result, true);
	}


}
