package testCase;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import system.Calendar;
import system.Day;
import system.EmploymentHistory;
import system.Position;
import system.Record;

public class EmploymentHistoryTest {
	
	class EmploymentHistoryStub extends EmploymentHistory{
		EmploymentHistoryStub(Day day, Position pos){
			super(day, pos);
		}

		@Override
		public double getSalary(int targetYear, int targetMonth,
				List<Record> records) {
			return 0;
		}

	}
	class DayStub extends Day{
		DayStub(int year, int month, int day){
			super(year, month, day);
		}
		

	}
	class PositionStub extends Position{
		PositionStub(){
			super("Software Engineer");
		}

		public boolean equals(Position pos){
			return true;
		}
	}

	@Test
	public void test_equals_true() {
		EmploymentHistory eh = new EmploymentHistoryStub(
				new DayStub(2015, 11, 24), new PositionStub());
		eh.setEndDate(new DayStub(2015, 11, 25));
		boolean result = eh.equals(
				new DayStub(2015, 11, 24), new DayStub(2015, 11, 25), new PositionStub());
		assertEquals(result, true);
	}
	@Test
	public void test_equals_false() {
		
		class DayStub2 extends Day{
			DayStub2(int year, int month, int day){
				super(year, month, day);
			}
			
		}
		class PositionStub2 extends Position{
			PositionStub2(){
				super("Software Engineer");
			}
		}
		
		EmploymentHistory eh = new EmploymentHistoryStub(
				new DayStub2(2015, 11, 24), new PositionStub2());
		eh.setEndDate(new DayStub2(2015, 11, 25));
		boolean result = eh.equals(
				new DayStub2(2015, 11, 24), null, new PositionStub2());
		assertEquals(result, false);
	}
	@Test
	public void test_isBetween_true_1(){
		EmploymentHistory eh = new EmploymentHistoryStub(
				new Day(2015, 11, 24), new PositionStub());
		boolean result = eh.isBetween(2015, 11);
		assertEquals(result, true);
	}
	@Test
	public void test_isBetween_true_2(){
		EmploymentHistory eh = new EmploymentHistoryStub(
				new Day(2015, 11, 1), new PositionStub());
		eh.setEndDate(new Day(2015, 11, 2));
		boolean result = eh.isBetween(2015, 11);
		assertEquals(result, true);
	}
	@Test
	public void test_isBetween_false(){
		EmploymentHistory eh = new EmploymentHistoryStub(
				new DayStub(2015, 11, 24), new PositionStub());
		eh.setEndDate(new DayStub(2015, 11, 25));
		boolean result = eh.isBetween(2015, 10);
		assertEquals(result, false);
	}
	@Test
	public void test_addRecord(){
		class RecordStub extends Record{
			public double getFullTimeSalaryAdjustment(int targetYear,int targetMonth,double basicSalary){
				return 2.0;
			}
			public double getPartTimeSalaryAdjustment(int targetYear,int targetMonth,double basicSalary){
				return 1.0;
			}
		}
		class RecordStub1 extends Record{
			public double getFullTimeSalaryAdjustment(int targetYear,int targetMonth,double basicSalary){
				return 5.0;
			}
			public double getPartTimeSalaryAdjustment(int targetYear,int targetMonth,double basicSalary){
				return 10.0;
			}
		}
		
		EmploymentHistory eh = new EmploymentHistoryStub(
				new DayStub(2015, 11, 24), new PositionStub());
		eh.addRecord(new RecordStub());
		eh.addRecord(new RecordStub1());
		eh.addRecord(new RecordStub1());
		List<Record> records = eh.getRecords();
		String result = "";
		for(Record r: records){
			double fullTimeSalary = r.getFullTimeSalaryAdjustment(2015, 1, 11);
			double partTimeSalary = r.getPartTimeSalaryAdjustment(2014, 1, 1);
			result += String.format("FT$:%.1f;PT$:%.1f;", fullTimeSalary, partTimeSalary);
		}
		assertEquals(result, "FT$:2.0;PT$:1.0;FT$:5.0;PT$:10.0;FT$:5.0;PT$:10.0;");
	}
}
