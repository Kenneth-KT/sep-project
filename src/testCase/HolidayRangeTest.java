package testCase;

import static org.junit.Assert.*;

import org.junit.*;

import system.*;

public class HolidayRangeTest {
	private HolidayRange holidayRange;
	private DayStub dayStart;
	private DayStub dayEnd;

	@Before
	public void setUp() throws Exception {
		holidayRange = new HolidayRange(dayStart, dayEnd);
	}

	class DayStub extends Day {
		public DayStub(int year,int month,int day){
			super(year,month,day);
		}
		public boolean isBetween(Day day1, Day day2){
			return true;
		}
		public String toString(){
			return "123";
		}
	}

	@Test
	public void testIsContain() {
		// It calls isBetween of Day instance with parameters equal to dayStart and dayEnd
		HolidayRange hr = new HolidayRange(Calendar.getInstance().getDay(2015, 11, 10),Calendar.getInstance().getDay(2015, 11, 12));
		assertEquals(true,hr.isContain(new DayStub(2015,11,10)));
	}
	
	@Test
	public void testIsRelated() {
		HolidayRange hr = new HolidayRange(Calendar.getInstance().getDay(2015, 11, 10),Calendar.getInstance().getDay(2015, 11, 12));
		assertEquals(true,hr.isRelated(2015));
	}
	@Test
	public void testToString(){
		HolidayRange hr = new HolidayRange(new DayStub(2015, 11, 10),new DayStub(2015, 11, 12));
		assertEquals("123 - 123",hr.toString());
	}
	@Test
	public void testGetStart() {
		String result;
		HolidayRange hr = new HolidayRange(new Day(2015, 11, 10),new Day(2015, 11, 12));
		result = hr.getStart().toString();
		assertEquals(result, "10-Nov-2015");
	}
	
}
