package system;


public interface PartTimeSalaryAdjustment {

	/**
	 * 
	 * @param hourlyWage
	 */


	abstract double getPartTimeSalaryAdjustment(int targetYear,int targetMonth,double hourlyWage);


}
