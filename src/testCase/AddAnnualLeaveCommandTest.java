package testCase;

import static org.junit.Assert.*;

import java.util.*;

import org.junit.Before;
import org.junit.Test;

import system.Employee;
import system.command.*;

public class AddAnnualLeaveCommandTest extends CommandTest{
	@Before
	public void setUp() throws Exception {
		this.cmd = AddAnnualLeave.getInstance();
		super.setUp();
	}

	@Test
	public void testExecuteCaseSuccess() {
		List<Employee> employees = new ArrayList<Employee>();
		Employee e1 = new Employee("Peter", null);
		Employee e2 = new Employee("Mary", null);
		employees.add(e1);
		employees.add(e2);
		String[] params = {"Peter", "2-1-2016","7-1-2016"};
		cmd.execute(employees, params );
		assertEquals("Added 2-Jan-2016 - 7-Jan-2016 [Annual Leave] to Peter",outContent.toString());
	}
	
	@Test
	public void testExecuteCaseNotFound() {
		List<Employee> employees = new ArrayList<Employee>();
		Employee e1 = new Employee("Peter", null);
		Employee e2 = new Employee("Mary", null);
		employees.add(e1);
		employees.add(e2);
		String[] params = {"Amy", "2-1-2016","7-1-2016"};
		cmd.execute(employees, params );
		assertEquals("Employee not Exist.", outContent.toString());
	}

	@Test
	public void testGetParamCaseSuccess() {
		String input = "Peter\n" +
						"2-1-2016\n" +
						"7-1-2016\n" ;
		String[] expected = {"Peter", "2-1-2016", "7-1-2016"}; 
		Scanner sc = new Scanner(input);
		String[] params;
		
		params = cmd.getParam(sc);
		assertEquals("Please enter the name of the corresponding employee: "+
					 "Please enter the start day(dd-MM-yyyy): "+
					 "Please enter the end day(dd-MM-yyyy): "
						, outContent.toString());
		assertArrayEquals(expected,params);
		
	}

}
