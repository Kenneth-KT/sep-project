package system;


public interface FullTimeSalaryAdjustment {

	/**
	 * 
	 * @param basicSalary
	 */
	abstract double getFullTimeSalaryAdjustment(int targetYear,int targetMonth,double basicSalary);

}
