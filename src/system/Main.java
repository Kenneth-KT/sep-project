package system;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import system.command.AddAnnualLeave;
import system.command.AddHoliday;
import system.command.AddNoPayLeave;
import system.command.AddSickLeave;
import system.command.Command;
import system.command.MarkHolidayOnCalendar;
import system.command.ShowHoliday;
import system.command.ShowLeaveRecord;
import system.command.ShowMarkedHoliday;
import system.command.ShowSalary;


public class Main {
	public static void main(String[] args){
		ArrayList<Command> commands = new ArrayList<Command>();
		List<Employee> employees = Main.loadEmployeeData();
		Main.loadHolidayData();
		commands.add(ShowSalary.getInstance());
		commands.add(AddHoliday.getInstance());
		commands.add(MarkHolidayOnCalendar.getInstance());
		commands.add(ShowHoliday.getInstance());
		commands.add(ShowMarkedHoliday.getInstance());
		commands.add(AddAnnualLeave.getInstance());
		commands.add(AddNoPayLeave.getInstance());
		commands.add(AddSickLeave.getInstance());
		commands.add(ShowLeaveRecord.getInstance());
		String line;
		Scanner sc = new Scanner(System.in);
		int input = 0;
		while(true){
			printCommandsList(commands);
			input = Integer.parseInt(sc.nextLine());
			if(input == -1){
				break;
			}else{
				try {
					commands.get(input).execute(employees, commands.get(input).getParam(sc));
				} catch (Exception ex) {
					System.out.println("Something's wrong with your input, please try again.");
				}
			}
		}
	}
	public static void printCommandsList(List<Command> commands){
		int index = -1;
		System.out.printf("\n%15s\t\t%60s","Command Number","Command");
		for(Command c:commands){
			index += 1;
			System.out.printf("\n%15d\t\t%60s",index,c.toString());
		}
		System.out.print("\n>");
			
	}
	public static List<Employee> loadEmployeeData(){
		Position engineer = new Position("Engineer");
		List<Employee> employees = new ArrayList<Employee>();
		Employee e = new Employee("Jason",Calendar.getInstance().getDay(1997, 1,1));
		EmploymentHistory eh = new FullTimeEmploymentHistory(Calendar.getInstance().getDay("01-01-2014"),engineer,10000);
		//System.out.println(Calendar.getInstance().getDay("31-12-2016"));
		eh.setEndDate(Calendar.getInstance().getDay("31-12-2016"));
		for(int i = 1;i<=31;i++){
			if(i == 2 || i == 3 || i == 4 ||i == 11 ||i == 15 ||i == 16 || i == 17||i ==18||i == 19 || i==24 ||i==25||i==31){
				//these are weekend and weekday
				//2-oct is no pay leave
				//10-oct work on non working day
				//1-oct are national day
				//15-oct are sick leave
				//16-oct - 19-oct are annual leave
				continue;
			}
			eh.addRecord(new AttendanceRecord(Calendar.getInstance().getDay(2015,10,i),9,0,18,0));
		}
		e.addEmploymentHistory(eh);
		employees.add(e);
		
		e = new Employee("Peter",Calendar.getInstance().getDay(1997, 1,1));
		eh = new PartTimeEmploymentHistory(Calendar.getInstance().getDay("01-01-2014"),engineer,100);
		eh.setEndDate(Calendar.getInstance().getDay("31-12-2015"));
		for(int i = 1;i<=31;i++){
			if(i == 2||i == 3 || i == 4 || i == 10 ||i == 11 ||i == 17||i ==18||i==24 ||i==25||i==31){
				//these are weekend and weekday
				continue;
			}
			eh.addRecord(new AttendanceRecord(Calendar.getInstance().getDay(2015,10,i),9,0,18,0));
		}
		e.addEmploymentHistory(eh);
		employees.add(e);
		return employees;
	}
	public static void loadHolidayData(){
		Holiday h = Calendar.getInstance().createHoliday("Christmas", "jingle bell jingle bell jingle all the way");
		if(h != null)
			Calendar.getInstance().markHoliday(h, 2015, 12, 25, 2015, 12, 27);
		h = null;
		h = Calendar.getInstance().createHoliday("Mid-Autumn Festival", "Time to have moon cake");
		if(h != null)
			Calendar.getInstance().markHoliday(h, 2015, 9, 26, 2015, 9, 27);
	}
}
