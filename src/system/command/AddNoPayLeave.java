package system.command;

import java.util.List;
import java.util.Scanner;

import system.Calendar;
import system.Day;
import system.Employee;
import system.NoPayLeave;

public class AddNoPayLeave implements Command {
	private static AddNoPayLeave instance;
	private AddNoPayLeave (){
		
	}
	public static AddNoPayLeave getInstance(){
		return (instance == null)?new AddNoPayLeave():instance;
	}
	@Override
	public void execute(List<Employee> employees, String[] param) {
		String start[] = param[1].split("-");
		String end[] = param[2].split("-");
		int startD = Integer.parseInt(start[0]);
		int startM = Integer.parseInt(start[1]);
		int startY = Integer.parseInt(start[2]);
		int endD = Integer.parseInt(end[0]);
		int endM = Integer.parseInt(end[1]);
		int endY = Integer.parseInt(end[2]);
		Day d1 = Calendar.getInstance().getDay(startY, startM, startD);
		Day d2 = Calendar.getInstance().getDay(endY, endM, endD);
		Employee target = null;
		for(Employee e:employees){
			if(e.match(param[0].trim()))
				target = e;
		}
		if(target != null){
			NoPayLeave al = new NoPayLeave(d1,d2);
			target.addNoPayLeaveRecord(al);
		}

	}

	@Override
	public String[] getParam(Scanner sc) {
		String param[] = new String[3];
		System.out.print("Please enter the name of the corresponding employee: ");
		param[0] = sc.nextLine();
		System.out.print("Please enter the start day(dd-MM-yyyy): ");
		param[1] = sc.nextLine();
		System.out.print("Please enter the end day(dd-MM-yyyy): ");
		param[2] = sc.nextLine();
		return param;
	}

	@Override
	public String toString() {
		return "Add a No-Pay Leave Record";
	}

}
