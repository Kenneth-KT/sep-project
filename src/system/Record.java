package system;
public abstract class Record implements FullTimeSalaryAdjustment,PartTimeSalaryAdjustment {

	/**
	 * 
	 * @param basicSalary
	 */
	public abstract double getFullTimeSalaryAdjustment(int targetYear,int targetMonth,double basicSalary);

	/**
	 * 
	 * @param hourlyWage
	 */
	public abstract double getPartTimeSalaryAdjustment(int targetYear,int targetMonth,double hourlyWage);

}
