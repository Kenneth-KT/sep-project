package system;


import java.util.ArrayList;
import java.util.List;

public class Holiday{

	private String name;
	private String description;
	private List<HolidayRange> holidayRanges;
	public Holiday(String name,String description){
		this.name = name;
		this.description = description;
		holidayRanges = new ArrayList<HolidayRange>();
	}
	public boolean isHoliday(Day day){
		for(HolidayRange h:holidayRanges)
			if(h.isContain(day))
				return true;
		return false;
	}
	public void addHolidayRange(Day start, Day end){
		holidayRanges.add(new HolidayRange(start,end));
	}
	public String getName(){
		return name;
	}
	public String getDescription(){
		return this.description;
	}
	public List<HolidayRange> getRelatedHolidayRange(int year){
		List<HolidayRange> target = new ArrayList<HolidayRange>();
		for(HolidayRange hr:this.holidayRanges){
			if(hr.isRelated(year))
				target.add(hr);
		}
		return target;
	}
	public boolean match(String name){
		return this.name.equalsIgnoreCase(name.trim());
	}
}