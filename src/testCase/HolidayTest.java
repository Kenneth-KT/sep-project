package testCase;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import system.Day;
import system.Holiday;
import system.HolidayRange;

public class HolidayTest {
	private Holiday holiday;

	@Before
	public void setUp() throws Exception {
		holiday = new Holiday("National Day", "China National Day");
	}

	@Test
	public void testGetDescription() {
		String result;
		result = holiday.getDescription();
		assertEquals(result, "China National Day");
	}

	@Test
	public void testGetRelatedHolidayRange() {
		List<HolidayRange> result;
		Day start2 = new Day(2014, 10, 1);
		Day end2 = new Day(2014, 10, 3);
		holiday.addHolidayRange(start2, end2);
		Day start = new Day(2015, 10, 1);
		Day end = new Day(2015, 10, 3);
		holiday.addHolidayRange(start, end);
		result = holiday.getRelatedHolidayRange(2015);
		assertEquals(result.get(0).toString(), "1-Oct-2015 - 3-Oct-2015");
	}

}
