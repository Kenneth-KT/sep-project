package testCase;

import static org.junit.Assert.*;
import system.Calendar;
import org.junit.Test;

import system.AttendanceRecord;

public class AttendanceRecordTest {

	@Test
	public void testGetFullTimeSalaryAdjustment_sat() {
		AttendanceRecord ar = new AttendanceRecord(Calendar.getInstance().getDay(2015, 10, 10), 9, 0, 18, 0);
		double result = ar.getFullTimeSalaryAdjustment(2015, 10, 10000);
		assertEquals(result,750,0);
	}
	@Test
	public void testGetFullTimeSalaryAdjustment_penalty() {
		AttendanceRecord ar = new AttendanceRecord(Calendar.getInstance().getDay(2015, 10, 9), 9, 0, 17, 59);
		double result = ar.getFullTimeSalaryAdjustment(2015, 10, 10000);
		assertEquals(result,0,1);
	}
	@Test
	public void testGetFullTimeSalaryAdjustment_over() {
		AttendanceRecord ar = new AttendanceRecord(Calendar.getInstance().getDay(2015, 10, 9), 9, 0, 18, 31);
		double result = ar.getFullTimeSalaryAdjustment(2015, 10, 10000);
		assertEquals(result,83.3,1);
	}
	@Test
	public void testGetPartTimeSalaryAdjustment_between() {
		AttendanceRecord ar = new AttendanceRecord(Calendar.getInstance().getDay(2015, 10, 9), 9, 0, 18, 31);
		double result = ar.getPartTimeSalaryAdjustment(2015, 10, 100);
		assertEquals(result,951.7,1);
	}
	@Test
	public void testGetPartTimeSalaryAdjustment_notBetween() {
		AttendanceRecord ar = new AttendanceRecord(Calendar.getInstance().getDay(2015, 10, 9), 9, 0, 18, 31);
		double result = ar.getPartTimeSalaryAdjustment(2015, 11, 100);
		assertEquals(result,0,0);
	}
}
