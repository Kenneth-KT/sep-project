package testCase;

import static org.junit.Assert.*;

import org.junit.Test;

import system.Calendar;
import system.Day;
import system.SickLeave;

public class SickLeaveTest {
	private Calendar calendar = Calendar.getInstance();
	@Test
	public void testGetFullTimeSalaryAdjuctmentIntIntDayDayDouble() {
		double result;
		SickLeave sl = new SickLeave(calendar.getDay(2015, 9, 1), calendar.getDay(2015, 9, 2));
		result = sl.getFullTimeSalaryAdjustment(2015, 9,calendar.getDay(2015, 9, 1),calendar.getDay(2015, 9, 2), 10000);
		assertEquals(result,0,0);
	}

	@Test
	public void testGetPartTimeSalaryAdjuctmentIntIntDayDayDouble() {
		double result;
		SickLeave sl = new SickLeave(calendar.getDay(2015, 9, 1), calendar.getDay(2015, 9, 2));
		result = sl.getPartTimeSalaryAdjustment(2015, 9,calendar.getDay(2015, 9, 1), calendar.getDay(2015, 9, 2), 80);
		assertEquals(result,0,0);
	}

	@Test
	public void testGetFullTimeSalaryAdjuctmentIntIntDouble() {
		double result;
		SickLeave sl = new SickLeave(calendar.getDay(2015, 9, 1), calendar.getDay(2015, 9, 2));
		result = sl.getFullTimeSalaryAdjustment(2015, 9, 10000);
		assertEquals(result,0,0);
	}

	@Test
	public void testGetPartTimeSalaryAdjuctmentIntIntDouble() {
		double result;
		SickLeave sl = new SickLeave(calendar.getDay(2015, 9, 1), calendar.getDay(2015, 9, 2));
		result = sl.getPartTimeSalaryAdjustment(2015, 9, 80);
		assertEquals(result,0,0);
	}
	
	@Test
	public void testToString() {
		String result;
		SickLeave sickLeave = new SickLeave(calendar.getDay(2015, 12, 15), calendar.getDay(2015, 12, 17));
		result = sickLeave.toString();
		assertEquals(result, "15-Dec-2015 - 17-Dec-2015 [Sick Leave]");
	}

}
