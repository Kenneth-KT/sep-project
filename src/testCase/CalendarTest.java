package testCase;

import static org.junit.Assert.*;
import java.util.List;
import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;
import system.Calendar;
import system.Day;
import system.Holiday;

public class CalendarTest extends TestCase {
	private Calendar calendar;
	@Before
	public void setUp() throws Exception {
		calendar = Calendar.getInstance();
	}

	@Test
	public void testGetInstance() {
		boolean result = false;
		result = calendar.getClass().equals(Calendar.class);
		assertEquals(result, true);
	}

	@Test
	public void testGetDayString() {
		boolean result = false;
		result = calendar.getDay("18-11-2015").toString().equals("18-Nov-2015");
		assertEquals(result, true);
	}
	
	@Test
	public void testGetDayString2() {
		Day result;
		result = calendar.getDay("18-11");
		assertEquals(result, null);
	}

	@Test
	public void testGetDayIntIntInt() {
		boolean result;
		result = calendar.getDay(2000, 1, 1).toString().equals("1-Jan-2000");
		assertEquals(result, true);
	}

	@Test
	public void testLastDayOfMonth() {
		boolean result;
		result = calendar.LastDayOfMonth(2015, 11).toString().equals("30-Nov-2015");
		assertEquals(result, true);
	}

	@Test
	public void testLastDayOfMonth2() {
		Day result;
		result = calendar.LastDayOfMonth(2015, 14);
		assertEquals(result, null);
	}
	
	@Test
	public void testLastDayOfMonth3() {
		Day result;
		result = calendar.LastDayOfMonth(2015, 2);
		assertEquals(result.toString(), "28-Feb-2015");
	}
	
	@Test
	public void testLastDayOfMonth4() {
		Day result;
		result = calendar.LastDayOfMonth(2012, 2);
		assertEquals(result.toString(), "29-Feb-2012");
	}

	@Test
	public void testIsLeapYear() {
		boolean result;
		result = Calendar.isLeapYear(2012);
		assertEquals(result, true);
	}
	
	@Test
	public void testIsLeapYear2() {
		boolean result;
		result = Calendar.isLeapYear(2000);
		assertEquals(result, true);
	}
	
	@Test
	public void testIsLeapYear3() {
		boolean result;
		result = Calendar.isLeapYear(2100);
		assertEquals(result, false);
	}

	@Test
	public void testValid() {
		boolean result;
		result = Calendar.valid(2012, 2, 30);
		assertEquals(result, false);
	}
	
	@Test
	public void testValid2() {
		boolean result;
		result = Calendar.valid(2012, 13, 30);
		assertEquals(result, false);
	}
	
	@Test
	public void testValid3() {
		boolean result;
		result = Calendar.valid(2012, 12, 0);
		assertEquals(result, false);
	}
	
	@Test
	public void testMarkHolidayHolidayDayDay() {
		boolean result;
		Holiday holiday = new Holiday("The first day of January", "New Year");
		Day start = calendar.getDay(2015, 1, 1);
		Day end = calendar.getDay(2015, 1, 2);
		calendar.markHoliday(holiday, start, end);
		result = holiday.isHoliday(calendar.getDay(2015, 1, 1));
		assertEquals(result, true);
	}

	@Test
	public void testMarkHolidayHolidayIntIntIntIntIntInt() {
		boolean result;
		Holiday holiday = new Holiday("Lunar New Year’s Day", "Lunar New Year");
		calendar.markHoliday(holiday, 2015, 2, 19, 2015, 2, 21);;
		result = holiday.isHoliday(calendar.getDay(2015, 2, 19));
		result = holiday.isHoliday(calendar.getDay(2015, 2, 20));
		result = holiday.isHoliday(calendar.getDay(2015, 2, 21));
		assertEquals(result, true);
	}

	@Test
	public void testAddHoliday() {
		boolean result;
		Holiday holiday = new Holiday("Labour Day", "Labour Day");
		Day start = calendar.getDay(2015, 5, 1);
		Day end= calendar.getDay(2015, 5, 1);
		holiday.addHolidayRange(start, end);
		calendar.addHoliday(holiday);
		Day labourDay = calendar.getDay(2015, 5, 1);
		result = Calendar.isHoliday(labourDay);
		assertEquals(result, true);
	}

	@Test
	public void testAddHoliday2() {
		boolean result;
		Holiday holiday = new Holiday("Labour Day", "Labour Day");
		Day start = calendar.getDay(2015, 5, 1);
		Day end= calendar.getDay(2015, 5, 1);
		holiday.addHolidayRange(start, end);
		calendar.addHoliday(holiday);
		Holiday labourDay = new Holiday("Labour Day", "Labour Day");
		int before = calendar.getHoliday().size();
		calendar.addHoliday(labourDay);
		int after = calendar.getHoliday().size();
		assertEquals(before, after);
	}
	
	@Test
	public void testCreateHoliday() {
		boolean result;
		Holiday nationalDay = calendar.createHoliday("China National Day", "National Day");
		result = nationalDay.getClass().equals(Holiday.class);
		assertEquals(result, true);	
	}
	
	@Test
	public void testCreateHoliday2() {
		Holiday result;
		calendar.createHoliday("National Day", "National Day");
		result = calendar.createHoliday("National Day", "National Day");
		assertEquals(result, null);
	}

	@Test
	public void testIsHoliday() {
		boolean result;
		Holiday holiday = new Holiday("Chung Yeung Festival", "Chung Yeung Festival");
		Day start = calendar.getDay(2015, 10, 21);
		Day end= calendar.getDay(2015, 10, 21);
		holiday.addHolidayRange(start, end);
		calendar.addHoliday(holiday);
		Day chungYeungFestival = calendar.getDay(2015, 10, 21);
		result = Calendar.isHoliday(chungYeungFestival);
		assertEquals(result, true);
	}
	
	public void testIsHoliday2() {
		boolean result;
		Holiday holiday = new Holiday("Chung Yeung Festival", "Chung Yeung Festival");
		Day start = calendar.getDay(2015, 10, 21);
		Day end= calendar.getDay(2015, 10, 21);
		holiday.addHolidayRange(start, end);
		calendar.addHoliday(holiday);
		Day chungYeungFestival = calendar.getDay(2015, 10, 20);
		result = Calendar.isHoliday(chungYeungFestival);
		assertEquals(result, false);
	}

	@Test
	public void testDayWithinTargetMonth() {
		int result;
		Day start = calendar.getDay(2015, 12, 5);
		Day end = calendar.getDay(2015, 12, 20);
		result = Calendar.dayWithinTargetMonth(2015, 12, start, end);
		assertEquals(result, 16);
	}
	
	@Test
	public void testDayWithinTargetMonth2() {
		int result;
		Day start = calendar.getDay(2015, 12, 5);
		Day end = calendar.getDay(2015, 12, 1);
		result = Calendar.dayWithinTargetMonth(2015, 4, start, end);
		assertEquals(result, -1);
	}
	
	@Test
	public void testDayWithinTargetMonth3() {
		int result;
		Day start = calendar.getDay(2015, 12, 5);
		Day end = calendar.getDay(2015, 12, 20);
		result = Calendar.dayWithinTargetMonth(2015, 4, start, end);
		assertEquals(result, 0);
	}
	
	@Test
	public void testDayWithinTargetMonth4() {
		int result;
		Day start = calendar.getDay(2015, 12, 5);
		Day end = calendar.getDay(2016, 1, 20);
		result = Calendar.dayWithinTargetMonth(2015, 12, start, end);
		assertEquals(result, 27);
	}
	
	@Test
	public void testDayWithinTargetMonth5() {
		int result;
		Day start = calendar.getDay(2015, 11, 5);
		Day end = calendar.getDay(2015, 12, 20);
		result = Calendar.dayWithinTargetMonth(2015, 12, start, end);
		assertEquals(result, 20);
	}
/*	
	@Test
	public void testGetHoliday() {
		List<Holiday> result;
		result = calendar.getHoliday();
		assertEquals(result.get(0).getName(), "Labour Day");
		assertEquals(result.get(1).getName(), "Chung Yeung Festival");
		assertEquals(result.get(2).getName(), "National Day");
	}
*/	
	@Test
	public void testFindNumberOfWorkingDayIntIntDayDay() {
		int result;
		Day start = calendar.getDay(2015, 12, 5);
		Day end = calendar.getDay(2015, 12, 20);
		result = calendar.findNumberOfWorkingDay(2015, 12, start, end);
		assertEquals(result, 10);
	}
	
	@Test
	public void testMain() {
		Calendar.main(null);
	}
}
