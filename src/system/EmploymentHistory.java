package system;
import java.util.ArrayList;
import java.util.List;



public abstract class EmploymentHistory {

	private Day startDate;
	private Day endDate;
	private Position pos;
	private ArrayList<Record> records;
	
	public EmploymentHistory(Day startDate, Position pos) {
		super();
		this.startDate = startDate;
		this.pos = pos;
		this.records = new ArrayList<Record>();
	}
	public void setStartDate(Day startDate) {
		this.startDate = startDate;
	}
	public void setEndDate(Day endDate) {
		this.endDate = endDate;
	}
	public void addRecord(Record r){
		this.records.add(r);
	}
	public boolean equals(Day startDate, Day endDate, Position pos){
		if(this.startDate.equals(startDate) && 
				this.endDate.equals(endDate) && 
				this.pos.equals(pos))
			return true;
		return false;
	}
	/**
	 * 
	 * @param targetMonth
	 */
	
	public double getSalary(int targetYear,int targetMonth){
		return getSalary(targetYear,targetMonth,records);
	}
	public boolean isBetween(int year,int month){
		//System.out.println("startDate:"+startDate);
		//System.out.println("endDate:"+endDate);
		if(endDate==null)
			return !(startDate.compareTo(Calendar.getInstance().LastDayOfMonth(year, month)) > 0);
		else {
			return !(startDate.compareTo(Calendar.getInstance().LastDayOfMonth(year, month)) > 0) && !(endDate.compareTo(Calendar.getInstance().getDay(year, month, 1)) < 0);
		}
	}
	public boolean isBetween(Day d){
		return d.isBetween(startDate, endDate);
	}
	public abstract double getSalary(int targetYear,int targetMonth, List<Record> records);
	public List<Record> getRecords(){
		return this.records;
	}
}
