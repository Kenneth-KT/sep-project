package testCase;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import system.AttendanceRecord;
import system.Calendar;
import system.Employee;
import system.EmploymentHistory;
import system.FullTimeEmploymentHistory;
import system.PartTimeEmploymentHistory;
import system.Position;

public class PartTimeEmploymentHistoryTest {

	@Test
	public void testGetSalary() {
		List<Employee> employees = new ArrayList<Employee>();
		Position programmer = new Position("Programmer");
		Employee e = new Employee("Johnson",Calendar.getInstance().getDay(1997, 1,1));
		PartTimeEmploymentHistory eh = new PartTimeEmploymentHistory(Calendar.getInstance().getDay("01-01-2014"),programmer,95);
		for(int i = 1;i<=31;i++){
			if(i == 2 || i == 3 || i == 4 || i == 10 ||i == 11 ||i == 16 || i == 17||i ==18||i == 19 || i==24 ||i==25||i==31){
				//these are weekend and weekday
				//2-oct is no pay leave
				//1-oct are national day
				//16-oct - 19-oct are annual leave
				continue;
			}
			eh.addRecord(new AttendanceRecord(Calendar.getInstance().getDay(2015,10,i),9,0,18,0));
		}
		e.addEmploymentHistory(eh);
		employees.add(e);
		assertEquals(eh.getSalary(2015, 10), 16245, 0);
	}
	
	
}
