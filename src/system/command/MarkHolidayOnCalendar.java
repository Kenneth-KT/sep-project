package system.command;

import java.util.List;
import java.util.Scanner;

import system.Calendar;
import system.Employee;
import system.Holiday;

public class MarkHolidayOnCalendar implements Command {
	private static MarkHolidayOnCalendar instance;
	private MarkHolidayOnCalendar (){
		
	}
	public static MarkHolidayOnCalendar getInstance(){
		return (instance == null)?new MarkHolidayOnCalendar():instance;
	}
	@Override
	public void execute(List<Employee> employees, String[] param) {
		String start[] = param[1].split("-");
		String end[] = param[2].split("-");
		int startD = Integer.parseInt(start[0]);
		int startM = Integer.parseInt(start[1]);
		int startY = Integer.parseInt(start[2]);
		int endD = Integer.parseInt(end[0]);
		int endM = Integer.parseInt(end[1]);
		int endY = Integer.parseInt(end[2]);
		Holiday targetHoliday = Calendar.getInstance().findHoliday(param[0]);
		if(targetHoliday != null)
			Calendar.getInstance().markHoliday(targetHoliday, startY, startM, startD, endY, endM, endD);
	}

	@Override
	public String[] getParam(Scanner sc) {
		String param[] = new String[3];
		System.out.print("Please enter the holiday to be marked: ");
		param[0] = sc.nextLine();
		System.out.print("Please enter the start day(dd-MM-yyyy): ");
		param[1] = sc.nextLine();
		System.out.print("Please enter the end day(dd-MM-yyyy): ");
		param[2] = sc.nextLine();
		return param;
	}
	@Override
	public String toString(){
		return "Mark the existing Holiday on the Calendar";
	}

}
