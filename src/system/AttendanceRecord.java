package system;


public class AttendanceRecord extends Record {

	private Day day;
	private int work_start_hour;
	private int work_start_minute;
	private int work_end_hour;
	private int work_end_minute;


	public AttendanceRecord(Day day, int work_start_hour,int work_start_minute, int work_end_hour, int work_end_minute) {
		this.day = day;
		this.work_start_hour = work_start_hour;
		this.work_start_minute = work_start_minute;
		this.work_end_hour = work_end_hour;
		this.work_end_minute = work_end_minute;
	}

	/**
	 * 
	 * @param basicSalary
	 */
	public double getFullTimeSalaryAdjustment(int targetYear,int targetMonth,double basicMonthlySalary) {
		int totalWorkMinute = (work_end_hour - work_start_hour)*60 + work_end_minute - work_start_minute;
		double overTimeMinute = 0;
		double penaltyMinute = 0;
		double hourlySalary = (basicMonthlySalary/20/9);
		DayOfWeek currentDayOfWeek = DayOfWeek.toDayOfWeek(day);
		if(day.isBetween(targetYear, targetMonth)){
			//this record is related to that month
			if(Calendar.isHoliday(day) || currentDayOfWeek == DayOfWeek.SATURDAY || currentDayOfWeek == DayOfWeek.SUNDAY){
				return Math.round(totalWorkMinute/60) * 1.5 * hourlySalary;
			}else{
				if(totalWorkMinute > 9 * 60){
					//compensated
					overTimeMinute = totalWorkMinute - 9 * 60;
				}else{
					//penalty
					penaltyMinute = 9 * 60 - totalWorkMinute;
				}
				return Math.round(overTimeMinute/60) * 1.5 * hourlySalary - Math.round(penaltyMinute/60) * hourlySalary;
			}
		}
		return 0;
	}

	/**
	 * 
	 * @param hourlyWage
	 */
	public double getPartTimeSalaryAdjustment(int targetYear,int targetMonth,double hourlyWage) {
		if(day.isBetween(targetYear, targetMonth)){
			//this record is related to that month
			return ((work_end_hour - work_start_hour) * 60.0 + (work_end_minute - work_start_minute))/60 * hourlyWage;
		}
		return 0;
	}
}
