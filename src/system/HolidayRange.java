package system;

public class HolidayRange {
	private Day start;
	private Day end;
	public HolidayRange(Day start, Day end) {
		this.start = start;
		this.end = end;
	}

	public boolean isContain(Day day){
		return day.isBetween(start, end);
	}
	public boolean isRelated(int year){
		return (start.isBetween(year) || end.isBetween(year));
	}
	public Day getStart(){
		return this.start;
	}
	@Override
	public String toString(){
		return start.toString() + " - " + end.toString();
	}
}
