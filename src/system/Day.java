package system;

public class Day implements Cloneable, Comparable<Day> {
	private static final String MonthNames = "JanFebMarAprMayJunJulAugSepOctNovDec";
	private int status;
	private int year;
	private int month;
	private int day;

	public Day(int year, int month, int day) {
		this.year = year;
		this.month = month;
		this.day = day;
	}

	@Override
	public String toString() {
		return day + "-" + MonthNames.substring((month - 1) * 3, month * 3)
				+ "-" + year;
	}

	@Override
	public Day clone() {
		Day copy = null;
		try {
			copy = (Day) super.clone();
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return copy;
	}

	public void next() {
		if (Calendar.valid(year, month, day + 1)) {
			day += 1;
		} else if (Calendar.valid(year, month + 1, 1)) {
			month += 1;
			day = 1;
		} else {
			year += 1;
			month = 1;
			day = 1;
		}
	}

	public void next(int dayIncreasement) {
		if (dayIncreasement == 0)
			return;
		else {
			next();
			next(dayIncreasement - 1);
		}
	}

	public void previous() {
		if (Calendar.valid(year, month, day - 1)) {
			day -= 1;
		} else if (Calendar.valid(year, month - 1, 31)) {
			month -= 1;
			day = 31;
		} else if (Calendar.valid(year, month - 1, 30)) {
			month -= 1;
			day = 30;
		} else if (Calendar.valid(year, month - 1, 29)) {
			month -= 1;
			day = 29;
		} else if (Calendar.valid(year, month - 1, 28)) {
			month -= 1;
			day = 28;
		} else {
			year -= 1;
			month = 12;
			day = 31;
		}
	}

	public int diffInDay(Day another) {

		// Day temp = another.clone();
		if (compareTo(another) > 0) {
			Day temp = another.clone();
			temp.next();
			return 1 + diffInDay(temp);
		} else if (compareTo(another) < 0) {
			Day temp = another.clone();
			temp.previous();
			return -1 + diffInDay(temp);
		} else {
			return 0;
		}
	}

	@Override
	public int compareTo(Day another) {
		// System.out.println(this + " compare with "+ another);
		// return this.day.compareTo(another.day);
		return (this.year - another.year)*365+(this.month - another.month)*31+this.day - another.day;
/*		if (this.year - another.year != 0)
			return this.year - another.year;
		else if (this.month - another.month != 0)
			return this.month - another.month;
		else {
			return this.day - another.day;
		}
*/

	}

	public boolean isBetween(Day day1, Day day2) {
		if (compareTo(day1) >= 0 && compareTo(day2) <= 0)
			return true;
		else if (compareTo(day2) >= 0 && compareTo(day1) <= 0)
			return true;
		else
			return false;
	}

	public boolean isBetween(int year, int month) {
		return (this.year == year && this.month == month) ? true : false;
	}
	public boolean isBetween(int year){
		return (this.year == year)?true:false;
	}

	@Override
	public boolean equals(Object obj) {
		Day temp = (Day) obj;
		if (obj != null && this.year == temp.year && this.month == temp.month
				&& this.day == temp.day)
			return true;
		return false;
	}

	public static void main(String[] args) {
//		System.out.println(Calendar.getInstance().getDay(2015, 10, 1));
//		System.out.println(Calendar.getInstance().getDay(2015, 3, 2)
//				.diffInDay(Calendar.getInstance().getDay(2015, 2, 1)));
	}

	public Day getNextDay() {
		Day newDay = null;
		newDay = Calendar.getInstance().getDay(year, month, day + 1);
		if (newDay != null)
			return newDay;
		else {
			newDay = Calendar.getInstance().getDay(year, month + 1, 1);
			if (newDay != null)
				return newDay;
			else {
				newDay = Calendar.getInstance().getDay(year + 1, 1, 1);
				if (newDay != null)
					return newDay;
				
			}
		}
		return newDay;
	}

}
