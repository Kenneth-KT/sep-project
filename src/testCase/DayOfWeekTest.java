package testCase;

import static org.junit.Assert.*;

import org.junit.Test;

import system.Day;
import system.DayOfWeek;

public class DayOfWeekTest {

	@Test
	public void test_toDayOfWeek_Monday(){
		Day day = new Day(2015, 11, 16);
		DayOfWeek dayOfWeek = DayOfWeek.toDayOfWeek(day);
		assertEquals(dayOfWeek, DayOfWeek.MONDAY);
	}
	
	@Test
	public void test_toDayOfWeek_Tuesday(){
		Day day = new Day(2015, 11, 17);
		DayOfWeek dayOfWeek = DayOfWeek.toDayOfWeek(day);
		assertEquals(dayOfWeek, DayOfWeek.TUESDAY);
	}
	
	@Test
	public void test_toDayOfWeek_Wednesday(){
		Day day = new Day(2015, 11, 18);
		DayOfWeek dayOfWeek = DayOfWeek.toDayOfWeek(day);
		assertEquals(dayOfWeek, DayOfWeek.WEDNESDAY);
	}
	
	@Test
	public void test_toDayOfWeek_Thursday(){
		Day day = new Day(2015, 11, 19);
		DayOfWeek dayOfWeek = DayOfWeek.toDayOfWeek(day);
		assertEquals(dayOfWeek, DayOfWeek.THURSDAY);
	}
	
	@Test
	public void test_toDayOfWeek_Friday(){
		Day day = new Day(2015, 11, 20);
		DayOfWeek dayOfWeek = DayOfWeek.toDayOfWeek(day);
		assertEquals(dayOfWeek, DayOfWeek.FRIDAY);
	}
	
	@Test
	public void test_toDayOfWeek_Saturday(){
		Day day = new Day(2015, 11, 21);
		DayOfWeek dayOfWeek = DayOfWeek.toDayOfWeek(day);
		assertEquals(dayOfWeek, DayOfWeek.SATURDAY);
	}
	
	@Test
	public void test_toDayOfWeek_Sunday(){
		Day day = new Day(2015, 11, 22);
		DayOfWeek dayOfWeek = DayOfWeek.toDayOfWeek(day);
		assertEquals(dayOfWeek, DayOfWeek.SUNDAY);
	}
	

}
