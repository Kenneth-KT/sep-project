
package system;
public class SickLeave extends LeaveRecord {
	public SickLeave(Day start,Day end){
		super(start, end);
	}
	/**
	 * 
	 * @param basicSalary
	 */
	@Override
	public double getFullTimeSalaryAdjustment(int targetYear, int targetMonth,Day start, Day end, double basicSalary) {
		return 0;
	}
	@Override
	public String toString(){
		return super.toString() + " ["+"Sick Leave"+"]";
	}

	/**
	 * 
	 * @param hourlyWage
	 */
	public double getPartTimeSalaryAdjustment(int targetYear,int targetMonth,Day start, Day end,double hourlyWage) {
		return 0;
	}

}