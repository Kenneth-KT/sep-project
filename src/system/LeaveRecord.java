package system;
import java.util.ArrayList;



public abstract class LeaveRecord extends Record {

	private Day start;
	private Day end;
	public LeaveRecord(Day start,Day end){
		this.start = start;
		this.end = end;
	}
	public Day getStart() {
		return start;
	}
	public Day getEnd() {
		return end;
	}
	@Override
	public String toString() {
		return start.toString() + " - " + end.toString();
	}
	public double getFullTimeSalaryAdjustment(int targetYear,int targetMonth,double basicSalary) {
		return getFullTimeSalaryAdjustment( targetYear, targetMonth,start,end, basicSalary);
	}
	public  double getPartTimeSalaryAdjustment(int targetYear,int targetMonth,double hourlyWage){
		return getPartTimeSalaryAdjustment( targetYear, targetMonth,start,end, hourlyWage);
	}
	public abstract double getFullTimeSalaryAdjustment(int targetYear,int targetMonth,Day start,Day end,double basicSalary);

	public abstract double getPartTimeSalaryAdjustment(int targetYear,int targetMonth,Day start,Day end,double hourlyWage);

}
