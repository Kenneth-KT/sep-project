package testCase;

import static org.junit.Assert.*;
import system.Calendar;
import system.Day;

import org.junit.Test;

public class DayTest {

	@Test
	public void testToString() {
		Day day = new Day(2015,11,10);
		String result = day.toString();
		assertEquals("10-Nov-2015",result);
	}


	@Test
	public void testDiffInDay_negative() {
		Day day1 = new Day(2015,11,10);
		Day day2 = new Day(2015,11,11);
		int diff = day1.diffInDay(day2);
		assertEquals(-1,diff);
	}
	@Test
	public void testDiffInDay_positive() {
		Day day1 = new Day(2015,11,11);
		Day day2 = new Day(2015,11,10);
		int diff = day1.diffInDay(day2);
		assertEquals(1,diff);
	}
	@Test
	public void testDiffInDay_equal() {
		Day day1 = new Day(2015,11,11);
		Day day2 = new Day(2015,11,11);
		int diff = day1.diffInDay(day2);
		assertEquals(0,diff);
	}

	@Test
	public void testCompareTo() {
		Day day1 = new Day(2015,11,11);
		Day day2 = new Day(2015,10,11);
		int result = day1.compareTo(day2);
		assertEquals(31,result);
	}

	@Test
	public void testIsBetweenDayDay() {
		Day day1 = new Day(2015,11,11);
		Day day2 = new Day(2015,11,20);
		Day target = new Day(2015,11,15);
		assertEquals(true,target.isBetween(day1, day2));
	}
	@Test
	public void testIsBetweenDayDay_reverse() {
		Day day1 = new Day(2015,11,11);
		Day day2 = new Day(2015,11,20);
		Day target = new Day(2015,11,15);
		assertEquals(true,target.isBetween(day2, day1));
	}
	@Test
	public void testIsBetweenDayDay_false() {
		Day day1 = new Day(2015,11,11);
		Day day2 = new Day(2015,11,20);
		Day target = new Day(2015,11,25);
		assertEquals(false,target.isBetween(day1, day2));
	}

	@Test
	public void testIsBetweenIntInt_true() {
		Day day1 = new Day(2015,11,11);
		assertEquals(true,day1.isBetween(2015, 11));
	}
	@Test
	public void testIsBetweenIntInt_false() {
		Day day1 = new Day(2015,11,11);
		assertEquals(false,day1.isBetween(2015, 10));
	}

	@Test
	public void testIsBetweenInt_true() {
		Day day1 = new Day(2015,11,11);
		assertEquals(true,day1.isBetween(2015));
	}
	@Test
	public void testIsBetweenInt_false() {
		Day day1 = new Day(2015,11,11);
		assertEquals(false,day1.isBetween(2014));
	}

	@Test
	public void testEqualsObject_true() {
		Day day1 = new Day(2015,11,11);
		Day day2 = new Day(2015,11,11);
		assertEquals(true,day1.equals(day2));
	}
	@Test
	public void testEqualsObject_false() {
		Day day1 = new Day(2015,11,11);
		Day day2 = new Day(2015,11,12);
		assertEquals(false,day1.equals(day2));
	}

	@Test
	public void testGetNextDay() {
		Day day1 = new Day(2015,11,11);
		assertEquals(day1.getNextDay(),Calendar.getInstance().getDay(2015,11,12));
	}
	
	@Test
	public void testGetNextDay2() {
		Day day1 = new Day(2015,12,31);
		assertEquals(day1.getNextDay(),Calendar.getInstance().getDay(2016,1,1));
	}
	
	@Test
	public void testGetNextDay3() {
		Day day1 = new Day(2015,11,30);
		assertEquals(day1.getNextDay(),Calendar.getInstance().getDay(2015,12,1));
	}
	
	@Test
	public void testPrevious() {
		Day day = new Day(2015, 12, 1);
		day.previous();
		assertEquals(day.toString(), "30-Nov-2015");
	}
	
	@Test
	public void testPrevious2() {
		Day day = new Day(2015, 11, 1);
		day.previous();
		assertEquals(day.toString(), "31-Oct-2015");
	}
	
	@Test
	public void testPrevious3() {
		Day day = new Day(2015, 3, 1);
		day.previous();
		assertEquals(day.toString(), "28-Feb-2015");
	}
	
	@Test
	public void testPrevious4() {
		Day day = new Day(2012, 3, 1);
		day.previous();
		assertEquals(day.toString(), "29-Feb-2012");
	}
	
	@Test
	public void testPrevious5() {
		Day day = new Day(2015, 1, 1);
		day.previous();
		assertEquals(day.toString(), "31-Dec-2014");
	}
	
	@Test
	public void testNext() {
		Day day = new Day(2015, 1, 31);
		day.next();
		assertEquals(day.toString(), "1-Feb-2015");
	}
	
	@Test
	public void testNext2() {
		Day day = new Day(2014, 12, 31);
		day.next();
		assertEquals(day.toString(), "1-Jan-2015");
	}
	
	@Test
	public void testNextInt() {
		Day day = new Day(2014, 12, 31);
		day.next(10);
		assertEquals(day.toString(), "10-Jan-2015");
	}
	
	@Test
	public void testMain() {
		Day.main(null);
	}

}
