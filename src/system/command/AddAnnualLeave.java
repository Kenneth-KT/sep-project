package system.command;

import java.util.List;
import java.util.Scanner;

import system.Calendar;
import system.AnnualLeave;
import system.Day;
import system.Employee;

public class AddAnnualLeave implements Command {
	private static AddAnnualLeave instance;
	private AddAnnualLeave (){
		
	}
	public static AddAnnualLeave getInstance(){
		return (instance == null)?new AddAnnualLeave():instance;
	}
	public void execute(List<Employee> employees, String[] param) {
		
		Day fromDay = Calendar.getInstance().getDay(param[1]);
		Day toDay = Calendar.getInstance().getDay(param[2]);
		Employee target = null;
		for(Employee e:employees){
			if(e.match(param[0].trim())) {
				target = e;
				break;
			}
		}
		if(target != null){
			AnnualLeave al = new AnnualLeave(fromDay,toDay);
			target.addAnnualLeaveRecord(al);
			System.out.print("Added "+ al +" to "+ target.getName());
		} else {
			System.out.print("Employee not Exist.");
		}

	}

	public String[] getParam(Scanner sc) {
		String param[] = new String[3];
		System.out.print("Please enter the name of the corresponding employee: ");
		param[0] = sc.nextLine();
		System.out.print("Please enter the start day(dd-MM-yyyy): ");
		param[1] = sc.nextLine();
		System.out.print("Please enter the end day(dd-MM-yyyy): ");
		param[2] = sc.nextLine();
		return param;
	}

	//@Override
	public String toString() {
		return "Add an Annual Leave Record";
	}
}
