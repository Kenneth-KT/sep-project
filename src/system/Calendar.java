package system;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
public class Calendar {
	private static Calendar instance;
	private static List<Day> days;
	private static List<Holiday> holidays;
	private Day createDay(int y, int m, int d) {
		Day newDay = null;
		if(valid(y,m,d)){
			newDay = new Day(y, m, d);
			for(Day existing:days)
				if(existing.equals(newDay)){
					return existing;
				}
			days.add(newDay);
		}
		return newDay;
	}
	
	private Calendar() {
		days = new ArrayList<Day>();
		holidays = new ArrayList<Holiday>();
	}

	public static Calendar getInstance() {
		instance = (instance == null) ? new Calendar() : instance;
		return instance;
	}
	public List<Holiday> getHoliday(){
		return holidays;
	}
	/**
	 * 
	 * @param time
	 * @return null will be returned if not in the format "DD-MM-YYYY" or invalid day
	 */
	public Day getDay(String time) {
		String[] parts = time.split("-");
		if (parts.length == 3) {
			int year = Integer.parseInt(parts[2].trim());
			int month = Integer.parseInt(parts[1].trim());
			int day = Integer.parseInt(parts[0].trim());
			return getDay(year, month, day);
		}
		return null;
	}

	public Day getDay(int year, int month, int day) {
		return createDay(year,month,day);
	}
	public Day LastDayOfMonth(int year,int month){
		if (month < 1 || month > 12)
			return null;
		switch (month) {
		case 1:
		case 3:
		case 5:
		case 7:
		case 8:
		case 10:
		case 12:
			return createDay(year,month,31);
		case 4:
		case 6:
		case 9:
		case 11:
			return createDay(year,month,30);
		case 2:
			if (isLeapYear(year))
				return createDay(year,month,29);
			else
				return createDay(year,month,28);
		}
		return null;		
	}
	

	public static boolean isLeapYear(int y) {
		if (y % 400 == 0)
			return true;
		else if (y % 100 == 0)
			return false;
		else if (y % 4 == 0)
			return true;
		else
			return false;
	}

	public static boolean valid(int y, int m, int d) {
		if (d < 1)
			return false;
		switch (m) {
		case 1:
		case 3:
		case 5:
		case 7:
		case 8:
		case 10:
		case 12:
			return d <= 31;
		case 4:
		case 6:
		case 9:
		case 11:
			return d <= 30;
		case 2:
			if (isLeapYear(y))
				return d <= 29;
			else
				return d <= 28;
		default :
			return false;
		}
	}
	public void markHoliday(Holiday holiday,Day start,Day end){
		holiday.addHolidayRange(start, end);
	}
	public void markHoliday(Holiday holiday,int startY,int startM,int startD,int endY,int endM,int endD){
		Day start = createDay(startY,startM,startD);
		Day end = createDay(endY,endM,endD);
		if(start != null && end != null)
			markHoliday(holiday,start,end);
	}
	
	public void addHoliday(Holiday holiday){
		if(holiday != null){
			if(findHoliday(holiday.getName()) != null)
				return;
			holidays.add(holiday);
		}
	}
	
	public Holiday findHoliday(String name){
		for(Holiday h:holidays){
			if(h.match(name))
				return h;
		}
		return null;
	}
	public Holiday createHoliday(String name,String description){
		if(findHoliday(name) != null)
			return null;
		Holiday h = new Holiday(name,description);
		holidays.add(h);
		return h;
	}
	public static boolean isHoliday(Day day){
		for(Holiday h:holidays)
			if(h.isHoliday(day))
				return true;
		return false;
	}
	public static int dayWithinTargetMonth(int targetYear,int targetMonth,Day start,Day end){
		Day targetEndDay = Calendar.getInstance().LastDayOfMonth(targetYear, targetMonth);
		Day targetStartDay = Calendar.getInstance().getDay(targetYear, targetMonth, 1);
		if(start.compareTo(end)>0)
			return -1;
		if(start.isBetween(targetStartDay, targetEndDay) && end.isBetween(targetStartDay, targetEndDay)){
			return end.diffInDay(start)+1;
		}else if(!start.isBetween(targetStartDay, targetEndDay) && !end.isBetween(targetStartDay, targetEndDay)){
			return 0;
		}else if(!start.isBetween(targetStartDay, targetEndDay) && end.isBetween(targetStartDay, targetEndDay)){
			return end.diffInDay(targetStartDay)+1;
		}else if(start.isBetween(targetStartDay, targetEndDay) && !end.isBetween(targetStartDay, targetEndDay)){
			return targetEndDay.diffInDay(start)+1;
		}
		return -1;
	}
	public static void main(String[] args){
//		System.out.println(Calendar.getInstance().getDay(2015,2,1));
//		System.out.println(Calendar.getInstance().getDay(2015,2,1));
//		System.out.println(Calendar.dayWithinTargetMonth(2015, 2, Calendar.getInstance().createDay(2015,2,1), Calendar.getInstance().createDay(2015,3,2)));
	}
	public int findNumberOfWorkingDay(int targetYear,int targetMonth,Day start,Day end){
		int count = 0;
		Day currentDay = start;
		while(currentDay.compareTo(end)<=0){
			if(currentDay.isBetween(targetYear, targetMonth) && !Calendar.isHoliday(currentDay) && DayOfWeek.SATURDAY != DayOfWeek.toDayOfWeek(currentDay) && DayOfWeek.SUNDAY != DayOfWeek.toDayOfWeek(currentDay))
				count++;
			currentDay = currentDay.getNextDay();
		}
		return count;
	}

}
