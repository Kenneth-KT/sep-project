package system.command;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

import system.Employee;
import system.HolidayRange;
import system.LeaveRecord;

public class ShowLeaveRecord implements Command {
	private static ShowLeaveRecord instance;
	private ShowLeaveRecord(){
		
	}
	public static ShowLeaveRecord getInstance(){
		return (instance == null)?new ShowLeaveRecord():instance;
	}
	@Override
	public void execute(List<Employee> employees, String[] param) {
		Employee target = null;
		for (Employee e : employees)
			if (e.match(param[0].trim()))
				target = e;

		String date[] = param[1].split("-");
		if (target != null)
			if (date.length == 2) {
				
				int month = Integer.parseInt(date[0]);
				int year = Integer.parseInt(date[1]);
				List<LeaveRecord> leaveRecords = target.getLeaveRecord(year,
						month);
				System.out.println(getHeader(target,year,month));
				if (leaveRecords != null)
					Collections.sort(leaveRecords,
							new Comparator<LeaveRecord>() {
								public int compare(LeaveRecord obj1,LeaveRecord obj2) {
									return obj1.getStart().compareTo(obj2.getStart());
								}
							}
					);
				for (LeaveRecord leaveRecord : leaveRecords)
					System.out.println(getRow(leaveRecord));
			}

	}
	private String getHeader(Employee e,int year, int month){
		return String.format("%s","All Leave Record(s) of " + e.getName() + " in " + month + "-" + year);
	}
	private String getRow(LeaveRecord leaveRecord){
		return String.format("\t%25s",leaveRecord.toString());
	}
	@Override
	public String toString(){
		return "Show Leave Record(s)";
	}
	@Override
	public String[] getParam(Scanner sc) {
		String[] params = new String[2];
		System.out.print("Please enter the name of the corresponding employee: ");
		params[0] = sc.nextLine();
		System.out.print("Please enter the month(MM-yyyy): ");
		params[1] = sc.nextLine();		
		return params;
	}

}
