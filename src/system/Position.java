package system;


public class Position {

	private String title;
	
	public Position(String title){
		this.title = title;
	}
	
	public boolean equals(Position pos){
		return this.title.equals(pos.title);
	}
	
	@Override
	public String toString(){
		return title;
	}
}
