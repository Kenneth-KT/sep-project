package system;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


public enum DayOfWeek {
	INVALID(0),
	MONDAY(1),
	TUESDAY(2),
	WEDNESDAY(3),
	THURSDAY(4),
	FRIDAY(5),
	SATURDAY(6),
	SUNDAY(7);
	
	private int numDayOfWeek;
	private DayOfWeek(int numDayOfWeek){
		this.numDayOfWeek = numDayOfWeek;
	}

	public static DayOfWeek toDayOfWeek(Day day) {
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);
		Date currentTime_2 = new Date();
		try {
			currentTime_2 = formatter.parse(day.toString());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		formatter = new SimpleDateFormat("u");
		String dayOfWeek = formatter.format(currentTime_2);
		int numDayOfWeek = Integer.parseInt(dayOfWeek);
		switch(numDayOfWeek){
			case 1:
				return MONDAY;
			case 2:
				return TUESDAY;
			case 3:
				return WEDNESDAY;
			case 4:
				return THURSDAY;
			case 5:
				return FRIDAY;
			case 6:
				return SATURDAY;
			case 7:
				return SUNDAY;
			default:
				return INVALID;
		}
		
	}

}
