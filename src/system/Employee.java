package system;
import java.util.ArrayList;
import java.util.List;



public class Employee {

	private String name;
	private Day dob;
	private ArrayList<EmploymentHistory> employmentHistorys;
	public Employee(String name,Day dob){
		this.name = name;
		this.dob = dob;
		this.employmentHistorys = new ArrayList<EmploymentHistory>();
	}
	public void addEmploymentHistory(EmploymentHistory eh){
		this.employmentHistorys.add(eh);
	}
	public String getName(){
		return name;
	}
	public Day getBirthday(){
		return dob;
	}
	public ArrayList<EmploymentHistory> getEmploymentHistorys() {
		return this.employmentHistorys;
	}
	public boolean match(String name){
		return this.name.equalsIgnoreCase(name);
	}
	/**
	 * 
	 * @param targetMonth
	 */
	public double getSalary(int targetYear,int targetMonth) {
		for(EmploymentHistory eh:employmentHistorys)
			if(eh.isBetween(targetYear, targetMonth))
				return eh.getSalary(targetYear, targetMonth);
		return 0;
	}
	public void addAnnualLeaveRecord(AnnualLeave al){
		this.addLeaveReocrd(al);
	}
	public void addSickLeaveRecord(SickLeave sl){
		this.addLeaveReocrd(sl);
	}
	public void addNoPayLeaveRecord(NoPayLeave npl){
		this.addLeaveReocrd(npl);
	}
	private void addLeaveReocrd(LeaveRecord lr){
		for(EmploymentHistory eh:employmentHistorys)
			if(eh.isBetween(lr.getStart()) || eh.isBetween(lr.getEnd())){
				eh.addRecord(lr);
			}
	}
	public List<LeaveRecord> getLeaveRecord(int year,int month){
		List<Record> records = null;
		List<LeaveRecord> leaveRecords = new ArrayList<LeaveRecord>();
		for(EmploymentHistory eh:employmentHistorys){
			if(eh.isBetween(year, month)){
				records = eh.getRecords();
				break;
			}
		}
		if(records != null){
			for(Record r:records){
				if(r instanceof LeaveRecord)
					leaveRecords.add((LeaveRecord)r);
			}
		}
		return leaveRecords;
		
	}

}
