package system.command;

import java.util.Formatter;
import java.util.List;
import java.util.Scanner;

import system.Calendar;
import system.Employee;
import system.Holiday;

public class ShowHoliday implements Command {
	private static ShowHoliday instance;
	private ShowHoliday(){
		
	}
	public static ShowHoliday getInstance(){
		return (instance == null)?new ShowHoliday():instance;
	}
	@Override
	public void execute(List<Employee> employees, String[] param) {
		List<Holiday> holidays = Calendar.getInstance().getHoliday();
		System.out.println(getHeader());
		for(Holiday h:holidays){
			System.out.println(print(h));
		}
	}

	@Override
	public String[] getParam(Scanner sc) {
		return new String[0];
	}
	private static String getHeader(){
		return String.format("%25s\t\t%50s","Name","Description");
	}
	private static String print(Holiday holiday){
		return String.format("%25s\t\t%50s", holiday.getName(),holiday.getDescription());
	}
	@Override
	public String toString(){
		return "Show all holiday";
	}

}
