package system;

public class AnnualLeave extends LeaveRecord {

	public AnnualLeave(Day start, Day end) {
		super(start, end);
	}

	/**
	 * 
	 * @param basicSalary
	 */
	public double getFullTimeSalaryAdjustment(int targetYear,int targetMonth,Day start,Day end,double basicSalary) {
		return 0;
	}

	/**
	 * 
	 * @param hourlyWage
	 */
	public double getPartTimeSalaryAdjustment(int targetYear,int targetMonth,Day start, Day end,double hourlyWage) {
		return 0;
	}
	@Override
	public String toString(){
		return super.toString() + " ["+"Annual Leave"+"]";
	}
}

