package testCase;

import static org.junit.Assert.*;

import java.util.*;

import org.junit.Before;
import org.junit.Test;

import system.Calendar;
import system.Employee;
import system.Holiday;
import system.command.AddHoliday;

public class AddHolidayCommandTest extends CommandTest{

	@Before
	public void setUp() throws Exception {
		cmd = AddHoliday.getInstance(); //TODO: error
		super.setUp();
	}

	@Test
	public void testExecuteCaseSuccess() {
		List<Employee> employees = new ArrayList<Employee>();
		String[] params = {"Christmas","Christmas 2015", "24-12-2015","26-12-2015"};
		cmd.execute(employees, params );
		Calendar c =Calendar.getInstance();
		
		Holiday h = c.getHoliday().get(0);
		
		assertEquals("Christmas",h.getName());
		assertEquals("Christmas 2015",h.getDescription());
	
		//param[0]: name,
		//param[1]: desc,
		//param[2]: startDay,
		//param[3]: endDay
		
	}
	
	@Test
	public void testGetParamCaseSuccess() {
		String input = "Christmas\n" +
						"Christmas 2015\n" +
						"24-12-2015\n" +
						"26-12-2015\n" ;
		String[] expected = {"Christmas", "Christmas 2015", "24-12-2015", "26-12-2015"}; 
		Scanner sc = new Scanner(input);
		String[] params;
		
		params = cmd.getParam(sc);
		assertEquals("Please enter the name of the holiday: "+
					 "Please enter the description of the holiday: "+
					 "Please enter the start day(dd-MM-yyyy): "+
					 "Please enter the end day(dd-MM-yyyy): "
						, outContent.toString());
		assertArrayEquals(expected,params );
		
	}


}
