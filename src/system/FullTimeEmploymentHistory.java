package system;

import java.util.List;



public class FullTimeEmploymentHistory extends EmploymentHistory {

	private double salary;
	public FullTimeEmploymentHistory(Day startDate, Position pos,double salary){
		super(startDate,pos);
		this.salary = salary;
	}
	public double getSalary(int targetYear,int targetMonth, List<Record> records) {
		
		double adjustedSalary = salary;
		//System.out.print(records.size());
		for(FullTimeSalaryAdjustment adjustment:records){
			adjustedSalary += adjustment.getFullTimeSalaryAdjustment(targetYear,targetMonth,salary);
		}
		return adjustedSalary;
	}

}
