package system.command;

import java.util.List;
import java.util.Scanner;

import system.Employee;



public interface Command {

	public void execute(List<Employee> employees,String[] param);
	public String[] getParam(Scanner sc);

}
