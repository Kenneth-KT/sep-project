package system.command;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

import system.Calendar;
import system.Employee;
import system.Holiday;
import system.HolidayRange;

public class ShowMarkedHoliday implements Command {
	private static ShowMarkedHoliday instance;
	private ShowMarkedHoliday(){
		
	}
	public static ShowMarkedHoliday getInstance(){
		return (instance == null)?new ShowMarkedHoliday():instance;
	}
	@Override
	public void execute(List<Employee> employees, String[] param) {
		List<Holiday> all = Calendar.getInstance().getHoliday();
		//Map<HolidayRange,Holiday> maps = new HashMap<HolidayRange,Holiday>();
		TreeMap<HolidayRange, Holiday> tmap = new TreeMap<HolidayRange, Holiday>(
				new Comparator<HolidayRange>() {

					public int compare(HolidayRange obj1, HolidayRange obj2) {
						return obj1.getStart().compareTo(obj2.getStart());
					}
				}
		);
		for(Holiday h:all){
			List<HolidayRange> hrs = h.getRelatedHolidayRange(Integer.parseInt(param[0]));
			for(HolidayRange hr:hrs){
				tmap.put(hr, h);
			}
		}
		System.out.println(this.getHeader());
		for(Map.Entry<HolidayRange, Holiday> entry:tmap.entrySet()){
			System.out.println(print(entry.getValue(),entry.getKey()));
		}
	}

	@Override
	public String[] getParam(Scanner sc) {
		String[] param = new String[1];
		System.out.print("Please enter the year(yyyy):");
		param[0] = sc.nextLine();
		return param;
	}
	private static String getHeader(){
		return String.format("%25s\t\t%30s", "Holiday","Date");
	}
	private static String print(Holiday h,HolidayRange hr){
		return String.format("%25s\t\t%30s", h.getName(),hr.toString());
	}
	@Override
	public String toString(){
		return "Show the detail of the Holiday within the target year";
	}

}
