
package system.command;

import java.util.List;
import java.util.Scanner;

import system.Calendar;
import system.Employee;
import system.Holiday;



public class AddHoliday implements Command {
	private static AddHoliday instance; 
	private AddHoliday(){
		
	}
	public static AddHoliday getInstance(){
		return (instance == null)?new AddHoliday():instance;
	}
	public void execute(List<Employee> employees,String[] param) {
		//param[0]: name,
		//param[1]: desc,
		//param[2]: startDay,
		//param[3]: endDay
		Calendar calendar = Calendar.getInstance();
		Holiday h = new Holiday(param[0], param[1]);
		h.addHolidayRange(calendar.getDay(param[2]), calendar.getDay(param[3]));
		calendar.addHoliday(h);
	}

	public String[] getParam(Scanner sc) {
		String[] params = new String[4];
		System.out.print("Please enter the name of the holiday: ");
		params[0] = sc.nextLine();
		System.out.print("Please enter the description of the holiday: ");
		params[1] = sc.nextLine();
		System.out.print("Please enter the start day(dd-MM-yyyy): ");
		params[2] = sc.nextLine();
		System.out.print("Please enter the end day(dd-MM-yyyy): ");
		params[3] = sc.nextLine();
		return params;

	}
	@Override
	public String toString(){
		return "Add Holiday";
	}
}
