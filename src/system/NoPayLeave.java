package system;

public class NoPayLeave extends LeaveRecord {
	public NoPayLeave(Day start,Day end){
		super(start,end);
	}
	/**
	 * 
	 * @param basicSalary
	 */
	@Override
	public double getFullTimeSalaryAdjustment(int targetYear, int targetMonth,Day start, Day end, double basicSalary) {
		return -1 * (Calendar.getInstance().findNumberOfWorkingDay(targetYear, targetMonth, start, end)*basicSalary/20);
	}

	/**
	 * 
	 * @param hourlyWage
	 */
	public double getPartTimeSalaryAdjustment(int targetYear,int targetMonth,Day start, Day end,double hourlyWage) {
		return 0;
	}
	@Override
	public String toString(){
		return super.toString() + " ["+"No-Pay Leave"+"]";
	}
/*	public static void main(String[] argv){
		NoPayLeave a = new NoPayLeave(Calendar.getInstance().getDay(2015, 11, 10));
		System.out.println(Calendar.getInstance().findNumberOfWorkingDay(2015,11,Calendar.getInstance().getDay(2015, 10, 30),Calendar.getInstance().getDay(2015, 11, 2)));
	}
*/
}