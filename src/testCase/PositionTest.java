package testCase;

import static org.junit.Assert.*;

import org.junit.Test;

import system.Day;
import system.Position;

public class PositionTest {
	private Position position;
	@Test
	public void testPosition() {
		boolean result;
		position = new Position("Software Engineer");
		result = position.getClass().equals(Position.class);
		assertEquals(result, true);
	}

	@Test
	public void testToString() {
		String result = null;
		position = new Position("Software Engineer");
		result = position.toString();
		assertEquals(result, "Software Engineer");
	}

}
