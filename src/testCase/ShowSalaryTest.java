package testCase;

import static org.junit.Assert.*;

import org.junit.Test;

import system.Calendar;
import system.command.ShowSalary;

public class ShowSalaryTest {
	private ShowSalary showSalary;
	@Test
	public void testGetInstance() {
		boolean result = false;
		showSalary = ShowSalary.getInstance();
		result = showSalary.getClass().equals(ShowSalary.class);
		assertEquals(result, true);
	}


	@Test
	public void testToString() {
		String result = null;
		ShowSalary ss = ShowSalary.getInstance(); 
		result = ss.toString();
		assertEquals(result, "Show Salary");
	}


}
