package system;

import java.util.List;



public class PartTimeEmploymentHistory extends EmploymentHistory {



	private double hourlyWage;
	
	
	public PartTimeEmploymentHistory(Day startDate, Position pos,double hourlyWage) {
		super(startDate, pos);
		this.hourlyWage = hourlyWage;
	}
	@Override
	public double getSalary(int targetYear,int targetMonth, List<Record> records) {
		double totalSalary = 0;
		for(PartTimeSalaryAdjustment adjustment:records){
			totalSalary += adjustment.getPartTimeSalaryAdjustment(targetYear,targetMonth,hourlyWage);
		}
		return totalSalary;
	}
}
