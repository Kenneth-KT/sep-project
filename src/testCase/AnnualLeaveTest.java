package testCase;

import static org.junit.Assert.*;

import org.junit.Test;

import system.AnnualLeave;
import system.Calendar;
import system.Day;

public class AnnualLeaveTest {

	@Test
	public void testToString() {
		AnnualLeave al = new AnnualLeave(Calendar.getInstance().getDay(2015, 11, 10),Calendar.getInstance().getDay(2015, 11, 10));
		assertEquals("10-Nov-2015 - 10-Nov-2015" + " ["+"Annual Leave"+"]",al.toString());
	}

	@Test
	public void testGetFullTimeSalaryAdjustmentIntIntDayDayDouble() {
		AnnualLeave al = new AnnualLeave(Calendar.getInstance().getDay(2015, 11, 10),Calendar.getInstance().getDay(2015, 11, 10));
		double result = al.getFullTimeSalaryAdjustment(2015, 11, Calendar.getInstance().getDay(2015, 11, 10), Calendar.getInstance().getDay(2015, 11, 10), 10000);
		assertEquals(0,result,0);
	}

	@Test
	public void testGetPartTimeSalaryAdjustmentIntIntDayDayDouble() {
		AnnualLeave al = new AnnualLeave(Calendar.getInstance().getDay(2015, 11, 10),Calendar.getInstance().getDay(2015, 11, 10));
		double result = al.getPartTimeSalaryAdjustment(2015, 11, Calendar.getInstance().getDay(2015, 11, 10), Calendar.getInstance().getDay(2015, 11, 10), 80);
		assertEquals(0,result,0);
	}

	@Test
	public void testAnnualLeave() {
		boolean result = false;
		AnnualLeave al = new AnnualLeave(Calendar.getInstance().getDay(2015, 11, 10),Calendar.getInstance().getDay(2015, 11, 10));
		result = al.getClass().equals(AnnualLeave.class);
		assertEquals(result, true);
	}

	@Test
	public void testGetFullTimeSalaryAdjustmentIntIntDouble() {
		AnnualLeave al = new AnnualLeave(Calendar.getInstance().getDay(2015, 11, 10),Calendar.getInstance().getDay(2015, 11, 10));
		double result = al.getFullTimeSalaryAdjustment(2015, 11, 10000);
		assertEquals(0,result,0);
	}

	@Test
	public void testGetPartTimeSalaryAdjustmentIntIntDouble() {
		AnnualLeave al = new AnnualLeave(Calendar.getInstance().getDay(2015, 11, 10),Calendar.getInstance().getDay(2015, 11, 10));
		double result = al.getPartTimeSalaryAdjustment(2015, 11, 80);
		assertEquals(0,result,0);
	}

	@Test
	public void testGetStart() {
		AnnualLeave al = new AnnualLeave(Calendar.getInstance().getDay(2015, 11, 10),Calendar.getInstance().getDay(2015, 11, 10));
		Day result = al.getStart();
		assertEquals(Calendar.getInstance().getDay(2015, 11, 10),result);
	}

	@Test
	public void testGetEnd() {
		AnnualLeave al = new AnnualLeave(Calendar.getInstance().getDay(2015, 11, 10),Calendar.getInstance().getDay(2015, 11, 10));
		Day result = al.getEnd();
		assertEquals(Calendar.getInstance().getDay(2015, 11, 10),result);
	}

}
